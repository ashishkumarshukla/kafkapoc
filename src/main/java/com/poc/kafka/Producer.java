package com.poc.kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
public class Producer {

	private static final String BOOTSTRAP_SERVER= "127.0.0.1:9092";
	private static final Logger LOG=LoggerFactory.getLogger(Producer.class);

	public static void main(String[] args) {

		Properties props = new Properties();
		props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,BOOTSTRAP_SERVER);
		props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,StringSerializer.class.getName());
		props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,StringSerializer.class.getName());
		
		KafkaProducer<String,String> producer = new KafkaProducer<>(props);
		
		for(int i=0;i<=10;i++) {
			
			 String value = "hello world " + i;
	         String key = "id_" + i;
	         LOG.info("Key :{}",key);
		ProducerRecord<String,String> record = 
				new ProducerRecord<>("third_topic",key,value);
		producer.send(record, (recordMetadata, ex) -> {
			if (ex == null) {
				LOG.info("Received new metadata= Topic {} Partition {} Offset {} Timestamp {}",
						recordMetadata.topic(),recordMetadata.partition(),
						recordMetadata.offset(),recordMetadata.timestamp());

			} else {
				LOG.info("Error while producing: {}",ex.getMessage());
			}

		});
		}
		producer.flush();
		producer.close();

	}
}