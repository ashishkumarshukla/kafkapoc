package com.poc.kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class Consumer {
	
	private static final String BOOTSTRAP_SERVER = "127.0.0.1:9092";
	private static final String GROUP_ID = "my-fifth-application";
	private static final String TOPIC = "third_topic";
    private static final Logger LOG=LoggerFactory.getLogger(Consumer.class);
	
	public static void main(String[] args) {

        KafkaConsumer<String, String> consumer = getKafkaConsumer();
        consumer.subscribe(Collections.singletonList(TOPIC));
        
        while(true){
            ConsumerRecords<String, String> records =
                    consumer.poll(Duration.ofMillis(100));

            for (ConsumerRecord<String, String> record : records){
                LOG.info("Key: {}  Value: {} Partition: {} Offset: {}" , record.key(),record.value(),record.partition(), record.offset());
            }
        }
       	
	}

    private static KafkaConsumer<String, String> getKafkaConsumer() {
        Properties props = new Properties();
        props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVER);
        props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.GROUP_ID_CONFIG, GROUP_ID);
        props.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        return new KafkaConsumer<>(props);
    }

    public static void assignAndSeek() {
        KafkaConsumer<String, String> consumer = getKafkaConsumer();

        int PARTITION = 0;
        TopicPartition partitionToReadFrom = new TopicPartition(TOPIC, PARTITION);
        long offsetToReadFrom = 15L;
        consumer.assign(Collections.singletonList(partitionToReadFrom));
        consumer.seek(partitionToReadFrom, offsetToReadFrom);
        int numberOfMessagesToRead = 5;
        boolean keepOnReading = true;
        int numberOfMessagesReadSoFar = 0;
        
        while(keepOnReading){
            ConsumerRecords<String, String> records =
                    consumer.poll(Duration.ofMillis(100));

            for (ConsumerRecord<String, String> record : records){
                numberOfMessagesReadSoFar ++;
                LOG.info("Key: {}  Value: {} Partition: {} Offset: {}" , record.key(),record.value(),record.partition(), record.offset());
                if (numberOfMessagesReadSoFar >= numberOfMessagesToRead){
                    keepOnReading = false; 
                    break;
                }
            }
        }

	}

}
